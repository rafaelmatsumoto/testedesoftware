import java.util.HashSet;
import java.util.Set;
/**
 *
 * @author rafael.matsumoto
 */
public class Triangulo {
    private double a;
    private double b;
    private double c;
    private double hipotenusa;
    Set<String> propriedades = new HashSet<>();
    
    public Triangulo(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
        
        this.setHipotenusa();
        
    }

    /**
     * @return the a
     */
    public double getA() {
        return a;
    }

    /**
     * @return the b
     */
    public double getB() {
        return b;
    }

    /**
     * @return the c
     */
    public double getC() {
        return c;
    }

    /**
     * @return the hipotenusa
     */
    public double getHipotenusa() {
        return this.hipotenusa;
    }

    /**
     * @param hipotenusa the hipotenusa to set
     */
    private void setValues() {
		setHipotenusa();

		if ((this.a + this.b + this.c - this.hipotenusa) > this.hipotenusa) {

			this.propriedades.add("V�lido");

			equilatero();

			addEscaleno();

			retangulo();

			} else {

			this.propriedades.add("Inv�lido");

			}
    }

	private void equilatero() {
		if (this.a == this.b || this.b == this.c || this.c == this.a) {

			if (this.a == this.b && this.b == this.c) {

				this.propriedades.add("Equil�tero");

			} else {

				this.propriedades.add("Is�sceles");

			}

		}
	}

	private void retangulo() {
		if (((this.a * this.a + this.b * this.b + this.c * this.c) - this.hipotenusa * this.hipotenusa) == this.hipotenusa

				* this.hipotenusa) {

			this.propriedades.add("Ret�ngulo");

		}
	}

	private void addEscaleno() {
		if (this.a != this.b && this.b != this.c && this.c != this.a) {

			this.propriedades.add("Escaleno");

		}
	}

	private void setHipotenusa() {
		this.hipotenusa = this.a;
		if (this.hipotenusa < this.b) {

			this.hipotenusa = this.b;

		}

		if (this.hipotenusa < this.c) {

			this.hipotenusa = this.c;

		}
	}
    
    public void getPropriedades(){
        this.propriedades.forEach((valor) -> {
                    System.out.println(valor);
        });
    }
}